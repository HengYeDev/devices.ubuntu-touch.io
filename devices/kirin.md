---
codename: 'kirin'
name: 'Sony Xperia 10'
comment: 'community device'
icon: 'phone'
maturity: .80
noinstall: true
---

## The ultimate wide

Ubuntu Touch on the Sony Xperia 10 is a great experience as the gesture based interface is easy to use on the brilliant 21:9 radio screen.

### Forum thread

- https://forums.ubports.com/topic/5258/sony-xperia-10-kirin

### Maintainers

- [HengYeDev](https://forums.ubports.com/user/hengyedev)

#### Kernel

- https://gitlab.com/ubports/community-ports/android9/sony-xperia-10/kernel-sony-kirin

#### Device

- https://gitlab.com/ubports/community-ports/android9/sony-xperia-10/sony-kirin


